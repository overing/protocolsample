﻿using System.IO;
using System.IO.Compression;

namespace NewProtocolDemo.Protocol
{
	public static class Compression
	{
		const int BufferSize = 4096;

		public static byte[] Compress(byte[] raw)
		{
			using (var memory = new MemoryStream())
			{
				using (var stream = new GZipStream(memory, CompressionMode.Compress, true))
				{
					stream.Write(raw, 0, raw.Length);
				}
				return memory.ToArray();
			}
		}

		public static byte[] Decompress(byte[] gzip, int offset, int len)
		{
			using (var stream = new GZipStream(new MemoryStream(gzip, offset, len), CompressionMode.Decompress))
			{
				var buffer = new byte[BufferSize];
				using (var memory = new MemoryStream())
				{
					while (true)
					{
						int count = stream.Read(buffer, 0, BufferSize);
						if (count > 0)
							memory.Write(buffer, 0, count);
						else
							return memory.ToArray();
					}
				}
			}
		}
	}
}
