﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace NewProtocolDemo.Protocol
{
	public enum BattleBuff : byte
	{
		Normal,
		Dizz,
		DefUp
	}

	public enum StrongType : byte
	{
		HP = 1,
		MP = 2,
		ATK = 4,
		DEF = 8,
		ALL = 15,
	}

	[Serializable]
	public class NotifyUpdate : ProtocolBase
	{
	}

	[Serializable]
	public class Hero : ProtocolBase
	{
		public int Level;
		public string Name;
		public int HP;
		public int MP;
	}

	[Serializable]
	public class StrongHero : ProtocolBase
	{
		public byte strongType;

		public Hero Hero;

		public StrongType StrongType { get { return (StrongType)strongType; } set { strongType = (byte)value; } }
	}

	[Serializable]
	public class BattleHero : Hero
	{
		public byte battleBuff;

		public BattleBuff BattleBuff { get { return (BattleBuff)battleBuff; } set { battleBuff = (byte)value; } }
	}

	[Serializable]
	public class Party : ProtocolBase
	{
		public Hero[] Hellos;
	}

	[Serializable]
	public class UpdateNotifys : ProtocolBase
	{
		public List<NotifyUpdate> NotifyList;
	}

	#region ProtocolBase

	[Serializable]
	public abstract class ProtocolBase
	{
		static readonly BinaryFormatter Formatter = new BinaryFormatter();
		static readonly Dictionary<Type, int> IDCache = new Dictionary<Type, int>();

		public int ID { get { return GetID(GetType()); } }

		public static int GetID<T>() where T : ProtocolBase
		{
			return GetID(typeof(T));
		}

		public static int GetID(Type type)
		{
			if (IDCache.ContainsKey(type))
				return IDCache[type];
			var sb = new StringBuilder(type.FullName);
			foreach (var field in type.GetFields())
				sb.Append(field.Name).Append(':').Append(field.FieldType.FullName);
			return IDCache[type] = sb.ToString().GetHashCode();
		}

		public static T FromBytes<T>(byte[] bytes, int offset, int count) where T : ProtocolBase
		{
			byte[] buffer = Compression.Decompress(bytes, offset, count);
			var memory = new MemoryStream(buffer, false);
			return Formatter.Deserialize(memory) as T;
		}

		public byte[] ToBytes()
		{
			var buffer = new MemoryStream();
			Formatter.Serialize(buffer, this);
			return Compression.Compress(buffer.ToArray());
		}

		public override string ToString()
		{
			var type = GetType();
			var sb = new StringBuilder(type.FullName);
			foreach (var field in type.GetFields())
			{
				sb.AppendLine();
				sb.Append('\t').Append(field.FieldType.Name).Append(' ');
				sb.Append(field.Name).Append('=').Append(field.GetValue(this));
			}
			return sb.ToString();
		}
	}

	#endregion
}
