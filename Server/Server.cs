﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using NewProtocolDemo.Protocol;

namespace NewProtocolDemo.Server
{
	class Server
	{
		TcpListener Listener;
		bool Running;

		public Server(int port)
		{
			Listener = new TcpListener(IPAddress.Any, port);
		}

		public void Start()
		{
			Listener.Start();
			Running = true;

			AcceptLoop();
		}

		void AcceptLoop()
		{
			while (Running)
			{
				var client = Listener.AcceptTcpClient();
				Task.Run(() => HandleConnection(client));
			}
		}

		void HandleConnection(TcpClient client)
		{
			Console.WriteLine("Connected");

			var buffer = new byte[4096];
			var stream = client.GetStream();
			int read = stream.Read(buffer, 0, buffer.Length);

			var protocol = ProtocolBase.FromBytes<ProtocolBase>(buffer, 0, read);
			if (protocol != null)
			{
				Console.WriteLine("{0}: {1}", DateTime.Now, protocol);

				if (protocol.ID == ProtocolBase.GetID<Party>())
				{
					var party = protocol as Party;
					if (party != null && party.Hellos != null && party.Hellos.Length > 0)
						Console.WriteLine(party.Hellos[0]);
				}
				else if (protocol.ID == ProtocolBase.GetID<BattleHero>())
				{
					var hero = protocol as BattleHero;
					if (hero != null)
						Console.WriteLine(hero);
				}
				else
					Console.WriteLine("Unhandle protocol: {0}", protocol);
			}
			else
				Console.WriteLine("Receive fail");
		}

		static void Main(string[] args)
		{
			new Server(8080).Start();
		}
	}
}
