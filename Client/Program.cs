﻿using System;
using System.IO;
using System.Net.Sockets;
using NewProtocolDemo.Protocol;

namespace NewProtocolDemo.Client
{
    class Client
    {
        static void Main(string[] args)
        {
            TcpClient client = null;
            try
            {

                var hero = new BattleHero { Level = 99, Name = Environment.UserName };
                hero.HP = 1000;
                hero.MP = 9629;
                hero.BattleBuff = BattleBuff.DefUp;

                var party = new Party { Hellos = new Hero[] { hero } };

				byte[] buffer = party.ToBytes();

//				Console.WriteLine(buffer.Length);

//				File.WriteAllBytes(@"C:\test.dat", buffer);

                client = new TcpClient("127.0.0.1", 8080);
                Stream stream = client.GetStream();
                stream.Write(buffer, 0, buffer.Length);
                stream.Flush();
            }
            finally
            {
                if (client != null)
                    client.Close();
            }
        }
    }
}
